package cookbook.domain;

import java.time.LocalDateTime;

public class Comment {
	
	public Comment(Long id, Long recipeId, Cook owner, String description, LocalDateTime timestamp) {
		this.id = id;
		actualid = id;
		this.recipeId = recipeId;
		this.owner = owner;
		this.description = description;
		this.timestamp = timestamp;
	}
	
	public Comment(Long recipeId, Cook owner, String description) {
		actualid++;
		this.id = actualid;
		this.recipeId = recipeId;
		this.owner = owner;
		this.description = description;
		this.timestamp = LocalDateTime.now();
	}

	Long id;
	Long recipeId;
    Cook owner;
    String description;
    LocalDateTime timestamp;
    static Long actualid;
    
    
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getRecipeId() {
		return recipeId;
	}

	public void setRecipeId(Long recipeId) {
		this.recipeId = recipeId;
	}

	public Cook getOwner() {
		return owner;
	}

	public void setOwner(Cook owner) {
		this.owner = owner;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}
	

	@Override
	public String toString() {
		return timestamp + "\n" + description;
	}
}
