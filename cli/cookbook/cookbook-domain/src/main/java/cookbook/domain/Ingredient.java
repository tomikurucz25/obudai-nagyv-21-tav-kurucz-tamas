package cookbook.domain;

public class Ingredient {
	double amount;
    String name;
    Unit unit;
	
    
    public Ingredient(double amount, Unit unit, String name) {
		this.amount = amount;
		this.unit = unit;
		this.name = name;
	}
    
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Unit getUnit() {
		return unit;
	}
	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	@Override
	public String toString() {
		return "Ingredient [amount=" + amount + ", name=" + name + ", unit=" + unit + "]";
	}
}
