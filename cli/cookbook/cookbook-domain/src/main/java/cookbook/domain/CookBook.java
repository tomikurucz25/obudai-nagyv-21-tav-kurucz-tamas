package cookbook.domain;

import java.util.ArrayList;
import java.util.List;

public class CookBook {

	public CookBook() {
		cooks = new ArrayList<Cook>();
		recipes = new ArrayList<Recipe>();
	}

	private long id;
	private List<Cook> cooks;
	private List<Recipe> recipes;

	public void setCooks(List<Cook> cooks) {
		this.cooks = cooks;
	}

	public List<Cook> getCooks() {
		return cooks;
	}

	public List<Recipe> getRecipes() {
		return recipes;
	}

	public void setRecipes(List<Recipe> recipes) {
		this.recipes = recipes;
	}
	
	
	
}
