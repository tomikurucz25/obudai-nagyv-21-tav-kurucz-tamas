package cookbook.domain;

public abstract class User {
	Long id;
	String username;
    String password;
	
    public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}    
    
}
