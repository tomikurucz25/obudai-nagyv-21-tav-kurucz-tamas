package cookbook.domain;

import java.util.ArrayList;
import java.util.List;

public class Recipe {

	public Recipe() {
		this.ingredients = new ArrayList<Ingredient>();
		this.categories = new ArrayList<Category>();
		this.comments = new ArrayList<Comment>();
	}
	
	
	
	public Recipe(Cook uploader, String name, String preparation,
			int servings, ArrayList<Ingredient> ingredients, ArrayList<Category> categories) {
		actualId++;
		this.id = actualId;
		this.uploader = uploader;
		this.name = name;
		this.preparation = preparation;
		this.servings = servings;
		
		this.ingredients = ingredients;
		this.categories = categories;
		this.comments = new ArrayList<Comment>();
	}



	Long id;
	Cook uploader;
	String name;
    ArrayList<Ingredient> ingredients;
    String preparation;
    int servings;
    ArrayList<Category> categories;
    ArrayList<Comment> comments;
    static Long actualId;
    
    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
		actualId = id;
	}
	
	public Cook getUploader() {
		return uploader;
	}
	public void setUploader(Cook uploader) {
		this.uploader = uploader;
	}
	public ArrayList<Ingredient> getIngredients() {
		return ingredients;
	}
	public void setIngredients(ArrayList<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}
	public ArrayList<Category> getCategories() {
		return categories;
	}
	public void setCategories(ArrayList<Category> categories) {
		this.categories = categories;
	}
	public ArrayList<Comment> getComments() {
		return comments;
	}
	public void setComments(ArrayList<Comment> comments) {
		this.comments = comments;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getServings() {
		return servings;
	}
	public void setServings(int servings) {
		this.servings = servings;
	}
	public String getPreparation() {
		return preparation;
	}
	public void setPreparation(String preparation) {
		this.preparation = preparation;
	}
	
	
	@Override
	public String toString() {
		String ingredientsString = "";
		for (Ingredient ingredient : ingredients) {
			ingredientsString += "\t" + ingredient.amount + " " + ingredient.unit + " " + ingredient.name + "\n";
		}
		
		String categoriesString = "";
		for (Category category: categories) {
			categoriesString += "\t" + category.toString() + "\n";
		}
		
		return "-- Recipe: " + this.name + " --\nRecipe ID:\t" + this.id + "\nUploader:\t" + this.uploader.username + "\nServings:\t" + this.servings + "\nIngredients:\n" + ingredientsString  + "Preparation:\n" + this.preparation + "Categories:\n" + categoriesString;
	}
	
	
	
}
