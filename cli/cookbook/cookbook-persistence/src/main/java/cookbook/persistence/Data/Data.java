package cookbook.persistence.Data;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import cookbook.domain.*;

@Component
public class Data {

	public static ArrayList<Cook> cooks;
	
	public Data() {
		cooks = readCooks();
	}
	
	public ArrayList<Cook> readCooks() {
		try {
			BufferedReader br = new BufferedReader(
					new FileReader("../cookbook-persistence/bin/txtFiles/cooks.txt"));
			ArrayList<Cook> result = new ArrayList<>();
			while (br.ready()) {
				String[] line = br.readLine().split(" ");
				Cook cook = new Cook(
						Long.parseLong(line[0]), line[1], line[2]);
				result.add(cook);
		    }
			br.close();
			return result;
		} catch (Exception e) {
			return null;
		}
	}
	
	public void readRecipes() {
		try {
			BufferedReader br = new BufferedReader(
					new FileReader("../cookbook-persistence/bin/txtFiles/recipes.txt"));
			ArrayList<Recipe> result = new ArrayList<>();
			String line = "";
			String attribute = "";
			Recipe recipe = new Recipe();
			ArrayList<Ingredient> ingredients = new ArrayList<Ingredient>();
			String preparation = "";
			ArrayList<Category> categories = new ArrayList<Category>();
			Cook actualCook = null;
			while (br.ready()) {
				line = br.readLine().toString();
				
				if (line.equals("[id]")) {
					if (recipe.getId() != null) {
						result.add(recipe);
						actualCook.getOwnRecipes().add(recipe);
					}
					recipe = new Recipe();
					attribute = "[id]";
					continue;
				}
				else if (line.equals("[user_id]")) {
					attribute = "[user_id]";
					continue;
				}
				else if (line.equals("[name]")) {
					attribute = "[name]";
					continue;
				}
				else if (line.equals("[ingredients]")) {
					ingredients = new ArrayList<Ingredient>();
					attribute = "[ingredients]";
					continue;
				}
				else if (line.equals("[preparation]")) {
					recipe.setIngredients(ingredients);
					preparation = "";
					attribute = "[preparation]";
					continue;
				}
				else if (line.equals("[servings]")) {
					recipe.setPreparation(preparation);
					attribute = "[servings]";
					continue;
				}
				else if (line.equals("[categories]")) {
					categories = new ArrayList<Category>();
					attribute = "[categories]";
					continue;
				}
				
				
				if (attribute == "[id]") {
					recipe.setId(Long.parseLong(line));
				}
				else if (attribute == "[user_id]") {
					Long userId = Long.parseLong(line);
					actualCook = cooks.stream().filter((u) -> u.getId() == userId).findFirst().orElse(null);
					recipe.setUploader(actualCook);
				}
				else if (attribute == "[name]") {
					recipe.setName(line);
				}
				else if (attribute == "[ingredients]") {
					String[] ingredientString = line.split(" ");
					Ingredient ingredient = new Ingredient(Double.parseDouble(ingredientString[0]), Unit.valueOf(ingredientString[1]), ingredientString[2]);
					ingredients.add(ingredient);
				}
				else if (attribute == "[preparation]") {
					preparation += line+"\n";
				}
				else if (attribute == "[servings]") {
					recipe.setServings(Integer.parseInt(line));
				}
				else if (attribute == "[categories]") {
					categories.add(Category.valueOf(line));
					recipe.setCategories(categories);
				}
			}
			if (!br.ready()&&recipe.getId() != null) {
				result.add(recipe);
				actualCook.getOwnRecipes().add(recipe);
			}
			br.close();
		} catch (Exception e) {
		}
	}
	
	public void readComments(){
		try {
			BufferedReader br = new BufferedReader(
					new FileReader("../cookbook-persistence/bin/txtFiles/comments.txt"));
			ArrayList<Comment> comments = new ArrayList<>();
			String line = "";
			while (br.ready()) {
				line = br.readLine();
				String[] commentString = line.split(" ");
				String commentMessage = "";
				for (int i = 4; i < commentString.length; i++) {
					commentMessage += commentString[i] + " ";
				}
				String recipeId = commentString[1];
				Cook actualCook = cooks.stream().filter((c) -> c.getId().equals(Long.parseLong(commentString[2]))).findFirst().orElse(null);
				
				ArrayList<Recipe> recipes = new ArrayList<Recipe>();
				for (Cook cook : cooks) {
					recipes.addAll(cook.getOwnRecipes());
				}
				Recipe actualRecipe = recipes.stream().filter((r) -> r.getId().equals(Long.parseLong(recipeId))).findFirst().orElse(null);
				Comment comment = new Comment(
						Long.parseLong(commentString[0]), Long.parseLong(commentString[1]), actualCook, commentMessage, LocalDateTime.parse(commentString[3]));
				comments.add(comment);
				actualCook.getComments().add(comment);
				actualRecipe.getComments().add(comment);
			}
			br.close();
		} catch (Exception e) {
		}
	}
	
	public void saveRecipe(Recipe recipe) {
		try {
			BufferedWriter bWriter = new BufferedWriter(
					new FileWriter("../cookbook-persistence/bin/txtFiles/recipes.txt", true));
			String ingredientsString = "";
			for (Ingredient ingredient : recipe.getIngredients()) {
				ingredientsString += ingredient.getAmount() + " " + ingredient.getUnit() + " " + ingredient.getName() + "\n";
			}
			String categoriesString = "";
			for (Category category: recipe.getCategories()) {
				categoriesString += "\n" + category.toString();
			}
			String data = "[id]\n" + recipe.getId() + "\n[user_id]\n" + recipe.getUploader().getId() + "\n[name]\n" + recipe.getName() + "\n[ingredients]\n" + ingredientsString + "[preparation]\n" + recipe.getPreparation() + "[servings]\n" + recipe.getServings() + "\n[categories]" + categoriesString;
			bWriter.write("\n" + data);
			bWriter.close();
		} catch (Exception e) {
			return;
		}
	}
	
	public void saveComment(Comment comment) {
		try {
			BufferedWriter bWriter = new BufferedWriter(
					new FileWriter("../cookbook-persistence/bin/txtFiles/comments.txt", true));
			String data = comment.getId() + " " + comment.getRecipeId() + " " + comment.getOwner().getId() + " " + comment. getTimestamp() + " " + comment.getDescription(); 
			bWriter.write("\n" + data);
			bWriter.close();
		} catch (Exception e) {
			return;
		}
	}
	
	public void deleteRecipe(String recipeId) {
		try {
			File inputFile = new File("../cookbook-persistence/bin/txtFiles/recipes.txt");
			File tempFile = new File("../cookbook-persistence/bin/txtFiles/recipestemp.txt");
			
			BufferedReader reader = new BufferedReader(new FileReader(inputFile));
			BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

			String currentLine;
			boolean nextLineIsTheId = false;
			boolean delete = false;
			boolean firstRow = true;			
			while((currentLine = reader.readLine()) != null) {
			    if (currentLine.equals("[id]")) {
					nextLineIsTheId = true;
					delete = false;
					continue;
				}
			    
			    if (nextLineIsTheId&&recipeId.equals(currentLine)) {
					delete = true;
			    	continue;
				}
			    else if (delete) {
					continue;
				}
			    else {
			    	if (nextLineIsTheId) {
			    		if (!firstRow) {
							writer.write("\n");
						}
			    		firstRow = false;
			    		writer.write("[id]");
			    		nextLineIsTheId = false;
					}
			    	writer.write("\n" + currentLine);
				}
			}
			writer.close(); 
			reader.close(); 
			inputFile.delete();
			tempFile.renameTo(inputFile);
		} catch (Exception e) {
		}
	}
	
	public void deleteComment(String recipeId) {
		try {
			File inputFile = new File("../cookbook-persistence/bin/txtFiles/comments.txt");
			File tempFile = new File("../cookbook-persistence/bin/txtFiles/commentstemp.txt");

			BufferedReader reader = new BufferedReader(new FileReader(inputFile));
			BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));
			
			String currentLine;
			boolean firstRow = true;
			while((currentLine = reader.readLine()) != null) {
			    if(recipeId.equals(currentLine.split(" ")[1])) continue;
			    if (!firstRow) {
					writer.write("\n");
				}
			    firstRow = false;
			    writer.write(currentLine);
			}
			writer.close(); 
			reader.close(); 
			inputFile.delete();
			tempFile.renameTo(inputFile);
		} catch (Exception e) {
		}
	}
}
