package cookbook.service;

import java.util.List;
import java.util.Set;
import cookbook.domain.*;

public interface ICookbookService {
	void login(String userData);
    void logout();
    void addRecipe(Recipe recipe);
    void saveComment(Recipe recipe, String commentMessage);
    boolean isLoggedIn();
    List<Recipe> getRecipes();
    Set<Category> getCategories();
    Cook getCurrentUser();
    boolean authenticate(User user);
    void deleteRecipe(String asd);
}
