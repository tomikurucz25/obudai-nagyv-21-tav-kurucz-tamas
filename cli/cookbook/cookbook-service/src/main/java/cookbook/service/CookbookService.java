package cookbook.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cookbook.domain.*;
import cookbook.service.*;
import cookbook.persistence.Data.*;

@Component
public class CookbookService implements ICookbookService {

	@Autowired
	Data data;
	
	boolean isLoggedIn = false;
	Cook loggedInCook = null;

	@Override
	public void login(String usernamePassword) {
		String[] userData = usernamePassword.split(" ");
		User user = new Cook(null, userData[0], userData[1]);
		
		if (authenticate(user)) {
			isLoggedIn = true;
		}
	}

	@Override
	public void logout() {
		isLoggedIn = false;
		loggedInCook =  null;
	}

	@Override
	public void addRecipe(Recipe recipe) {
		Cook cook = recipe.getUploader();
		cook.getOwnRecipes().add(recipe);
		data.saveRecipe(recipe);
	}

	@Override
	public void saveComment(Recipe recipe, String commentMessage) {
		Cook actualCook = getCurrentUser();
		Comment comment = new Comment(recipe.getId(), actualCook, commentMessage);
		actualCook.getComments().add(comment);
		recipe.getComments().add(comment);
		
		data.saveComment(comment);
	}

	@Override
	public boolean isLoggedIn() {
		return isLoggedIn;
	}

	@Override
	public List<Recipe> getRecipes() {
		ArrayList<Cook> cooks = data.cooks;
		ArrayList<Recipe> recipes = new ArrayList<Recipe>();
		for (Cook cook : cooks) {
			recipes.addAll(cook.getOwnRecipes());
		}
		return recipes;
	}

	@Override
	public Set<Category> getCategories() {
		return null;
	}

	@Override
	public Cook getCurrentUser() {
		return loggedInCook;
	}

	@Override
	public boolean authenticate(User user) {
		for (Cook c : data.cooks) {
			if (c.getUsername().equals(user.getUsername())&&c.getPassword().equals(user.getPassword())) {
				loggedInCook = c;
				return true;
			}
		}
		return false;
	}

	@Override
	public void deleteRecipe(String recipeId) {
		data.deleteRecipe(recipeId);
		data.deleteComment(recipeId);
	}

	public void deleteComment(Long recipeId) {
		ArrayList<Comment> comments = getRecipebyId(recipeId).getComments();
		for (Comment comment : comments) {
			comment.getOwner().getComments().remove(comment);
		}
	}

	public void readDataFromFiles()
	{
		data.readRecipes();
		data.readComments();
	}

	public Recipe getRecipebyId(Long recipeId) {
		List<Recipe> recipes = getRecipes();
		Recipe recipe = recipes.stream().filter((r) -> r.getId().equals(recipeId)).findFirst().orElse(null);
		return recipe;
	}
}
