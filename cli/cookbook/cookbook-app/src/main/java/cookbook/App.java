package cookbook;



import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

import cookbook.domain.*;
import cookbook.service.*;

@SpringBootApplication
public class App {

	@Autowired
	View view;
	
	@Autowired
	CookbookService cookbookService;

	public void start()
	{
		readDataFromFiles();
		view.printWelcome();
		view.printGuestOptions();
		String input = view.getInput();
		processMainMenuInput(input);
	}

	public void processMainMenuInput(String input) {
		if (!cookbookService.isLoggedIn()) {
			if (input.equals("1")) {
				processPostLoginInput();
				if (!cookbookService.isLoggedIn()) {
					view.printGuestOptions();
					input = view.getInput();
					processMainMenuInput(input);
				}
				else {
					view.printUserOptions();
					input = view.getInput();
					processMainMenuInput(input);
				}
			}
			else if (input.equals("2")) {
				processRecipeListingMenuInput();
			}
			else if (input.equals("Q")) {
				System.out.println("exit");
				System.exit(0);
			}
			else {
				System.out.println("Try it again!");
				view.printGuestOptions();
				input = view.getInput();
				processMainMenuInput(input);
			}
		} 
		else {
			if (input.equals("1")) {

				newRecipe();
				
				view.printUserOptions();
				input = view.getInput();
				processMainMenuInput(input);
			}
			else if (input.equals("2")) {
				processRecipeListingMenuInput();
			}
			else if (input.equals("3")) {
				deleteRecipe();
				view.printUserOptions();
				input = view.getInput();
				processMainMenuInput(input);
			}
			else if (input.equals("4")) {
				Cook loggedInCook = cookbookService.getCurrentUser();
				logout();
				view.printLogout(loggedInCook);
				view.printGuestOptions();
				input = view.getInput();
				processMainMenuInput(input);
			}
			else {
				System.out.println("Try it again!");
				view.printUserOptions();
				input = view.getInput();
				processMainMenuInput(input);
			}
		}
		
	}
	
	public void processPostLoginInput() {
		String usernamePassword = readCredentials();
		String[] userData = usernamePassword.split(" ");
		
		cookbookService.login(usernamePassword);
		
		if (cookbookService.isLoggedIn()) {
			view.printLogin(userData[0]);
		}
		else {
			view.printIncorrectCredentials();
			view.printNotAuthenticated();
		}
	}

	public void logout() {
		cookbookService.logout();
	}

	public void login() {
		processPostLoginInput();
	}
	
	public String readCredentials() {
		
		User user = view.readUser();
		String usernamePassword = user.getUsername() + " " + user.getPassword();
		return usernamePassword;
		
	}
	
	public void listRecipes() {
		List<Recipe> recipes = cookbookService.getRecipes();
		view.printRecipes(recipes);
	}
	
	public void printRecipe(int id) {
		view.printRecipe(getRecipeById(Long.parseLong(String.valueOf(id))));
	}
	
	public void processRecipeMenuInput(Recipe recipe, String input) {
		if (input.equals("1")) {
			view.printRecipeComments(recipe);
			if (!cookbookService.isLoggedIn()) {
				view.printGuestRecipeOptions();
				String guestRecipeInput = view.getInput();
				processRecipeMenuInput(recipe, guestRecipeInput);
			}
			else {
				view.printUserRecipeOptions();
				String userRecipeInput = view.getInput();
				processRecipeMenuInput(recipe, userRecipeInput);
			}
			
		}
		else if (input.equals("2")) {
			if (!cookbookService.isLoggedIn()) {
				processPostLoginInput();
				if (!cookbookService.isLoggedIn()) {
					view.printGuestRecipeOptions();
					input = view.getInput();
					processRecipeMenuInput(recipe,input);
				}
			}
			newComment(recipe);
		}
		else if (input.equals("Q")) {
			processRecipeListingMenuInput();
		}
		else {
			System.out.println("Try it again!");
			view.printGuestRecipeOptions();
			input = view.getInput();
			processRecipeMenuInput(recipe, input);
		}
		
	}
	
	public void newComment(Recipe recipe) {
		view.printNewCommentForm();
		String description = view.getInput();
		cookbookService.saveComment(recipe, description);
		view.printUserRecipeOptions();
		String input = view.getInput();
		processRecipeMenuInput(recipe, input);
	}

	public void newRecipe() {
		Recipe newRecipe = view.readRecipe(cookbookService.getCurrentUser());
		view.printCreatedRecipeDetails(newRecipe);
		cookbookService.addRecipe(newRecipe);
	}

	public void deleteRecipe() {
		Long recipeId = view.printDeleteRecipe();
		if (!recipeId.equals(null)) {
			Cook cook = getRecipeById(recipeId).getUploader();
			cookbookService.deleteComment(recipeId);
			cook.getOwnRecipes().remove(getRecipeById(recipeId));
			cookbookService.deleteRecipe(recipeId.toString());
		}
	}

	public void readDataFromFiles() {
		cookbookService.readDataFromFiles();
	}

	public boolean isDigit(String input) {
		for (Character character : input.toCharArray()) {
			if (!Character.isDigit(character)) {
				return false;
			}
		}
		return true;
	}

	public Recipe getRecipeById(Long recipeId) {
		return cookbookService.getRecipebyId(recipeId);
	}

	public void processRecipeListingMenuInput() {
		if (!cookbookService.isLoggedIn()) {
			listRecipes();
			System.out.println("Q: Go back");
			String recipeId = view.getInput();
			if (recipeId.equals("Q")) {
				view.printGuestOptions();
				String input = view.getInput();
				processMainMenuInput(input);
			}
			else {
				if (isDigit(recipeId)) {
					printRecipe(Integer.parseInt(recipeId));
					view.printGuestRecipeOptions();
					String guestRecipeInput = view.getInput();
					Recipe recipe = getRecipeById(Long.parseLong(recipeId));
					processRecipeMenuInput(recipe, guestRecipeInput);
				}
				else {
					System.out.println("Nincs ilyen azonosító.");
					processRecipeListingMenuInput();
				}
			}
		}
		else {
			listRecipes();
			System.out.println("Q: Go back");
			String recipeId = view.getInput();
			if (recipeId.equals("Q")) {
				view.printUserOptions();
				String input = view.getInput();
				processMainMenuInput(input);
			}
			else {
				if (isDigit(recipeId)) {
					printRecipe(Integer.parseInt(recipeId));
					view.printUserRecipeOptions();
					String userRecipeInput = view.getInput();
					Recipe recipe = getRecipeById(Long.parseLong(recipeId));
					processRecipeMenuInput(recipe, userRecipeInput);
				}
				else {
					System.out.println("Nincs ilyen azonosító.");
					processRecipeListingMenuInput();
				}
			}
		}
		}
		
}
