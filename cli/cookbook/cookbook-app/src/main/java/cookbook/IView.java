package cookbook;

import java.util.List;
import cookbook.domain.*;

public interface IView {
	Recipe readRecipe(Cook asd);
    void printWelcome();
    void printUserOptions();
    void printGuestOptions();
    void printRecipe(Recipe recipe);
    void printUserRecipeOptions();
    void printGuestRecipeOptions();
    void printRecipeComments(Recipe recipe);
    void printRecipes(List<Recipe> recipes);
    void printNewCommentForm();
    void printNotAuthenticated();
    void printLogout(Cook cook);
    String getInput();
    User readUser();
    void printIncorrectCredentials();
    String readRecipeId();
}
