package cookbook;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.springframework.stereotype.Component;

import cookbook.domain.*;

@Component
public class View implements IView {

	@Override
	public Recipe readRecipe(Cook cook) {
		System.out.println("What's the name of your dish?");
		String name = getInput();
		System.out.println("How many people does this dish serve?");
		int serving  = Integer.parseInt(getInput());
		
		ArrayList<Ingredient> ingredients = new ArrayList<Ingredient>();
		boolean addAnotherIngredient = true;
		while (addAnotherIngredient) {
			System.out.println("What kind of ingredients do you need?");
			double amount  = Double.parseDouble(getInput());
			Unit unit = Unit.valueOf(getInput());
			String ingredientName = getInput();
			Ingredient ingredient = new Ingredient(amount, unit, ingredientName);
			ingredients.add(ingredient);
			
			boolean wrongInput = true;
			while (wrongInput) {
				wrongInput = false;
				System.out.println("Add another? (Y/N)");
				String choice = getInput();
				if (choice.equals("N")||choice.equals("n")) {
					addAnotherIngredient = false;
				}
				else if (choice.equals("Y")||choice.equals("y")) {
					addAnotherIngredient = true;
				}
				else {
					wrongInput = true;
				}
			}	
		}

		System.out.println("How do you make this dish? (type 'C' to continue)");
		boolean next = false;
		String preparation = "";
		String line = "";
		while (!next) {
			line = getInput();
			if (line.equals("c")||line.equals("C")) {
				next = true;
			}
			else {
				preparation += line + "\n";
			}
		}
		
		System.out.println("How would you categorize this dish? (type 'C' to continue)");
		next = false;
		ArrayList<Category> categories = new ArrayList<Category>();
		while (!next) {
			for (int i = 0; i < Category.values().length; i++) {
				System.out.println(i + ": " + Category.values()[i]);
			}
			line = getInput();
			if (line.equals("c")||line.equals("C")) {
				next = true;
			}
			else {
				categories.add(Category.values()[Integer.parseInt(line)]);
			}
		}
		
		Recipe recipe = new Recipe(cook, name, preparation, serving, ingredients, categories);
		
		return recipe;
	}

	@Override
	public void printWelcome() {
		System.out.println("-- Application started --");
        System.out.println("-- Welcome to the Cookbook application! --");
		
	}

	@Override
	public void printUserOptions() {
		System.out.println("1: Create new recipe.");
		System.out.println("2: List existing recipes.");
		System.out.println("3: Delete recipe.");
		System.out.println("4: Log out.");
	}

	@Override
	public void printGuestOptions() {
		System.out.println("1: Log in.");
        System.out.println("2: Browse existing recipes.");
        System.out.println("Q: Exit the application.");
        
	}

	@Override
	public void printRecipe(Recipe recipe) {
		System.out.println(recipe);
	}

	@Override
	public void printUserRecipeOptions() {
		System.out.println("1: See comments");
		System.out.println("2: Write comment");
		System.out.println("Q: Go back");
	}

	@Override
	public void printGuestRecipeOptions() {
		System.out.println("1: See comments");
		System.out.println("2: -- Log in to write comments --");
		System.out.println("Q: Go back");
	}

	@Override
	public void printRecipeComments(Recipe recipe) {
		int idx = 1;
		for (Comment comment : recipe.getComments()) {
			System.out.println(idx + ".\t" + comment);
			idx++;
		}
	}

	@Override
	public void printRecipes(List<Recipe> recipes) {
		for (Recipe recipe : recipes) {
			System.out.println(recipe.getId() + ": " + recipe.getName().toString());
		}
		
	}

	@Override
	public void printNewCommentForm() {
		System.out.println("Write your comment (single-line):");
	}

	@Override
	public void printNotAuthenticated() {
		System.out.println("Authentication failed");
	}

	@Override
	public void printLogout(Cook cook) {
		System.out.println("-- " + cook.getUsername() + "user logged out --");
	}

	@Override
	public String getInput() {
		Scanner in = new Scanner(System.in);
		String input = in.nextLine();
		return input;
	}

	@Override
	public User readUser() {
		System.out.println("Give me your username:");
		String username = getInput();
		System.out.println("Give me your password:");
		String password = getInput();

		User user = new Cook(null, username, password);
		return user;
	}

	@Override
	public void printIncorrectCredentials() {
		System.out.println("Username or password is incorrect, please try again!");
	}

	@Override
	public String readRecipeId() {
		String recipeId = getInput();
		return recipeId;
	}

	public Long printDeleteRecipe() {
		System.out.println("Enter the id of the recipe you want to delete:");
		Long recipeId = Long.parseLong(getInput());
		System.out.println("Are you sure? (Y/N)");
		String input = getInput();
		if (input.equals("y")||input.equals("Y")) {
			return recipeId;
		}
		else {
			return null;
		}
	}

	public void printCreatedRecipeDetails(Recipe recipe) {
		System.out.println("-- Recipe created with the following informations: --");
		String text = "Name:\t" + recipe.getName() + "\nRecipe ID:\t" + recipe.getId() + "\nServings:\t" + recipe.getServings() + "\nUploader:\t" + recipe.getUploader().getUsername();
		System.out.println(text);
	}

	public void printLogin(String username) {
		System.out.println("-- " + username + "user logged in --");
	}
}
