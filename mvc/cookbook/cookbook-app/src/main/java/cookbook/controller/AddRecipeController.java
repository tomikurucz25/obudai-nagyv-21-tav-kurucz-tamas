package cookbook.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import cookbook.domain.Category;
import cookbook.persistence.entity.Recipe;
import cookbook.service.CookbookService;
import cookbook.service.dto.AddRecipeRequest;
import cookbook.service.transformer.RecipeTransformer;

@Controller
public class AddRecipeController {

	@Autowired
	private CookbookService cookbookService;
	
	@Autowired
	private RecipeTransformer recipeTransformer;
	
	@GetMapping("addRecipe")
	public String AddRecipeController()
	{
		return "add-recipe";
	}
	
	@PostMapping("addRecipe")
	public String addRecipe(@Valid AddRecipeRequest addRecipeRequest, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return "add-recipe";
		}
		
		Recipe recipe = new Recipe();
		recipeTransformer.transform(addRecipeRequest, recipe);
		cookbookService.addRecipe(recipe);
		
		return "redirect:recipes";
	}
	
	@ModelAttribute("categories")
	public Category[] getCategories() {
		return Category.values();
	}
}
