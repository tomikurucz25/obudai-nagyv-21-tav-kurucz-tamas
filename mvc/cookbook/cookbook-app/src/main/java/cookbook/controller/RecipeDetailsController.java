package cookbook.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import cookbook.service.CookbookService;
import cookbook.service.dto.RecipeDetailsView;
import cookbook.service.transformer.RecipeTransformer;

@Controller
public class RecipeDetailsController {

	@Autowired
	CookbookService cookbookService;
	
	@Autowired
	RecipeTransformer recipeTransformer;
	
	@GetMapping("recipe-details/{id}")
	public String details(@PathVariable("id" ) Long id, Model model) {
		RecipeDetailsView recipeDetailsView = new RecipeDetailsView();
		recipeTransformer.transform(cookbookService.getRecipebyId(id),recipeDetailsView);
		
		model.addAttribute("recipe", recipeDetailsView);
		return "recipe-details";
	}
}
