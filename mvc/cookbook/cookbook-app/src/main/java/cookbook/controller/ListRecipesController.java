package cookbook.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.catalina.startup.HomesUserDatabase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import cookbook.domain.Category;
import cookbook.service.CookbookService;
import cookbook.service.dto.RecipeFilters;
import cookbook.service.dto.RecipeListView;
import cookbook.service.transformer.RecipeTransformer;

@Controller
public class ListRecipesController {

	@Autowired
	private CookbookService cookbookService;
	
	@Autowired
	private RecipeTransformer recipeTransformer;
	
	@GetMapping("/")
	public String Home() {
		return "redirect:recipes";
	}
	
	@GetMapping("recipes")
	public String ListRecipes(Model model)
	{
		List<RecipeListView> recipeListViews = new ArrayList<RecipeListView>();
		recipeTransformer.transform(cookbookService.getRecipes(), recipeListViews);
		
		model.addAttribute("recipes", recipeListViews);
		return "recipes";
	}
	
	@PostMapping("recipes")
	public String listRecipes(RecipeFilters recipeFilters, Model model) {
		List<RecipeListView> recipeListViews = new ArrayList<RecipeListView>();
		recipeTransformer.transform(cookbookService.filteredRecipes(recipeFilters), recipeListViews);
		
		model.addAttribute("recipes", recipeListViews);
		return "recipes";
	}
}
