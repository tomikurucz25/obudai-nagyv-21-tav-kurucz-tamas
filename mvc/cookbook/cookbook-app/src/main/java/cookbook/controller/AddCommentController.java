package cookbook.controller;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import cookbook.persistence.entity.Comment;
import cookbook.persistence.entity.Recipe;
import cookbook.service.CookbookService;
import cookbook.service.dto.AddCommentRequest;
import cookbook.service.dto.AddRecipeRequest;

@Controller
public class AddCommentController {

	@Autowired
	CookbookService cookbookService;
	
	@GetMapping("addComment")
	public String addComment()
	{
		return "redirect:recipes";
	}
	
	@PostMapping("addComment")
	public String addComment(AddCommentRequest addCommentRequest) {
		cookbookService.saveComment(cookbookService.getRecipebyId(addCommentRequest.getRecipeId()), addCommentRequest.getCommentMessage());
		
		return "redirect:recipe-details/"+addCommentRequest.getRecipeId();
	}
}
