package cookbook.controller;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import cookbook.persistence.entity.Cook;
import cookbook.persistence.entity.Recipe;
import cookbook.persistence.repository.CookRepository;
import cookbook.service.CookbookService;
import cookbook.service.dto.MyRecipesView;
import cookbook.service.dto.RecipeListView;
import cookbook.service.transformer.RecipeTransformer;

@Controller
public class MyRecipesController {

	@Autowired
	private CookbookService cookbookService;
	
	@Autowired
	private RecipeTransformer recipeTransformer;
	
	
	
	@Autowired
	private CookRepository cookRepository;
	
	
	@GetMapping("myRecipes")
	public String AddRecipeController(Model model)
	{
		List<RecipeListView> recipeListViews = new ArrayList<>();
		List<Recipe> recipes = cookbookService.ActualUserRecipes();
		
		recipeTransformer.transform(recipes, recipeListViews);
		
		model.addAttribute("recipes", recipeListViews);
		return "myRecipes";
	}
	
	@GetMapping("deleteRecipe/{id}")
	public String deleteRecipe(@PathVariable("id") Long id, Model model) {
		cookbookService.deleteRecipe(id.toString());
		List<MyRecipesView> myRecipesViews = new ArrayList<>();
		
		
		//cookbookService.getCurrentUser().getOwnRecipes().forEach(r -> myRecipesViews.add(new MyRecipesView(r.getId(), r.getName(), r.getServings(), r.getUploader())));
		
		//model.addAttribute("recipes", myRecipesViews);
		return "redirect:/myRecipes";
	}
}
