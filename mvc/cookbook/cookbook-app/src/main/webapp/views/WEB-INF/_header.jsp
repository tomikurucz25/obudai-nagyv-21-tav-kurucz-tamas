<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="../css/menu.css">
    </head>
    <body>
    <nav>
	<a href="/recipes">Recipes</a>
	<a href="/myRecipes">My recipes</a>
	<a href="/addRecipe">New Recipe</a>
	<a href="/logout">Logout</a>
	<div class="animation start-home"></div><br>
	</nav><br/><br/>
