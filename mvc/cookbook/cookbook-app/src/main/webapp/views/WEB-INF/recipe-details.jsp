<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" type="text/css" href="../css/table.css">

<jsp:include page="_header.jsp"/>
	
<table class="styled-table">
	<colgroup>
       <col span="1" style="width: 20%;">
       <col span="1" style="width: 80%;">
    </colgroup>
	
	<thead>
		<tr>
			<th>${recipe.name}</th>
			<th></th>
		</tr>
	</thead>
			
	<tbody>
		<tr>
			<td>Preparation</td>
			<td>${recipe.preparation}</td>
		</tr>
		<tr>
			<td>Ingredients</td>
			<td>
				<c:forEach items="${recipe.ingredients}" var="ingredient">
					${ingredient.amount} ${ingredient.unit} ${ingredient.name}<br/>
				</c:forEach>
			</td>
		</tr>
	
		<tr>
			<td>Categories</td>
			<td>
				<c:forEach items="${recipe.categories}" var="category">
					${category},  
				</c:forEach>
			</td>
		</tr>
		<tr>
			<td>Servings</td>
			<td>${recipe.servings}</td>
		</tr>
		<tr>
			<td>Uploader</td>
			<td>${recipe.uploader.username}</td>
		</tr>

		
	</tbody>
</table><br/>

<table class="styled-table">
	<thead>
		<tr><th>Comments</th></tr>
	</thead>
			
	<tbody>	
		<table class="styled-table">
			<colgroup>
       			<col span="1" style="width: 30%;">
       			<col span="1" style="width: 30%;">
  				<col span="1" style="width: 70%;">
    		</colgroup>
			<tr  height="60">
				<th align="left">Time</th>
				<th align="left">Uploader</th>
				<th align="left">Comment</th>
			</tr>
			<c:forEach items="${recipe.comments}" var="comment">
				<tr>
					<td>${comment.timestamp}</td>
					<td>${comment.owner.username}</td>
					<td>${comment.description}</td>
				</tr>
			</c:forEach>
		</table>
		
	</tbody>		
</table><br/>

<table class="styled-table">
	<thead>
		<tr>
			<th>Write comment</th>
		</tr>
	</thead>
	
	<tbody>
		<tr>
			<td>
				<form action="/addComment" method="post">
					<input type="hidden" id="recipeId" name="recipeId" value="${recipe.id}">
					<input type="text" placeholder="Write your comment here..." id="commentMessage" name="commentMessage">
					<input type="submit" value="Save comment">
				</form>
			</td>
		</tr>
	</tbody>
	
</table>

<jsp:include page="_footer.jsp"/>