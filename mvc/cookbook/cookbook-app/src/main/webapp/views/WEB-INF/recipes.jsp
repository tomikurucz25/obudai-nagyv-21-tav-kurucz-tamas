<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<link rel="stylesheet" type="text/css" href="css/table.css">

<jsp:include page="_header.jsp"/>
		
<br/>
<div class="searchForm" align="center">
	<form  action="/recipes" method="post">
		<label for="searchInput">Search</label>
		<input type="text" id="searchInput" name="searchInput">
		<input type="submit" value="Search"><br>
		<input type="checkbox" id="name" name="name">
		<label for="name">name</label>
		<input type="checkbox" id="category" name="category">
		<label for="category">category</label>
		<input type="checkbox" id="ingredient" name="ingredient">
		<label for="ingredient">ingredient</label>
		<input type="checkbox" id="uploader" name="uploader">
		<label for="uploader">uploader</label>
	</form>
</div>
		
<table class="styled-table">
	<thead>
		<tr>
			<th>Name</th>
			<th>Categories</th>
			<th>Servings</th>
			<th>Uploader</th>
		</tr>
	</thead>

	<tbody>
		<c:forEach items="${recipes}" var="recipe">
			<tr>
				<td><a href="recipe-details/${recipe.id}">${recipe.name}</a></td>
				<td><c:forEach items="${recipe.categories}" var="category">
					${category},  
				</c:forEach></td>
				<td>${recipe.servings}</td>
				<td>${recipe.uploader.username}</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
		
<jsp:include page="_footer.jsp"/>