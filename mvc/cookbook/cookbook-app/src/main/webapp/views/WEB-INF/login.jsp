<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="css/login.css">
    </head>
		<div class="login-box">
  <h2>Login</h2>
  <c:if test="${param.error != null}">
			<div>
				Invalid username and password.
			</div>
		</c:if>
		<c:if test="${param.logout != null}">
			<div>
				You have been logged out.
			</div>
		</c:if>
  <form:form id="myform" action="login" method='POST'>
    <div class="user-box">
      <input type="text" name="username"/>
      <label>Username</label>
    </div>
    <div class="user-box">
      <input type="password" name="password"/>
      <label>Password</label>
    </div>
    <a onclick="document.getElementById('myform').submit()">
   	  <span></span>
      <span></span>
      <span></span>
      <span></span>
      Submit
    </a>
  </form:form>
</div>
    </body>
</html>