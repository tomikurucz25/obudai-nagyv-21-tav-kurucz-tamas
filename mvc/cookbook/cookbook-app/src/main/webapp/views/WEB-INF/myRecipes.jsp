<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" type="text/css" href="css/table.css">

<jsp:include page="_header.jsp"/>
		
<br/>
<table class="styled-table">
	<thead>
		<tr>
			<th>Name</th>
			<th>Servings</th>
			<th>Uploader</th>
			<th></th>
		</tr>
	</thead>
	
	<tbody>
		<c:forEach items="${recipes}" var="recipe">
			<tr>
				<td>${recipe.name}</td>
				<td>${recipe.servings}</td>
				<td>${recipe.uploader.username}</td>
				<td><div align="center"><a class="deleteButton" onclick="window.location='deleteRecipe/${recipe.id}'" target="_blank" rel="nofollow noopener">Delete</a></div></td>
				
			</tr>
		</c:forEach>
	</tbody>	
</table>
		
<jsp:include page="_footer.jsp"/>