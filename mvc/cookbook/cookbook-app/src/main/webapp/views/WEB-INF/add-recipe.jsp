<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<link rel="stylesheet" type="text/css" href="css/form.css">

<jsp:include page="_header.jsp"/>

<br/>
<div class="container">
  <form:form modelAttribute="addRecipeRequest" action="/addRecipe" method="post">
    <input type="hidden" id="id" name="id">
    <div class="row">
      <h2>Add recipe</h2>
      <div class="input-group">
       	<form:errors path="name"/>
       	<input type="text" placeholder="Name" id="name" name="name">
      </div>
      <div class="input-group">
        <form:errors path="servings"/>
        <input type="number" placeholder="Servings" id="servings" name="servings">
      </div>
      <div class="input-group input-group-icon">
        <form:errors path="ingredients"/>
        <textarea id="ingredients" placeholder="Ingredients" name="ingredients" rows="4" cols="67"></textarea>
      </div>
      <div class="input-group input-group-icon">
        <form:errors path="preparation"/>
        <textarea placeholder="Preparation" id="preparation" name="preparation" rows="4" cols="67"></textarea>
      </div>
      <div class="input-group input-group-icon">
      	<form:errors path="categories"/>
      	<select name="categories" id="categories" multiple>
  			<c:forEach items="${categories}" var="category">
				<option value="${category}">${category}</option>
			</c:forEach >	
	    </select>
	  </div>
	  <div>
	  	<input text-align="center" type="submit" value="Save recipe">
	  </div>
    </div>
  </form:form>
</div>
		
<jsp:include page="_footer.jsp"/>