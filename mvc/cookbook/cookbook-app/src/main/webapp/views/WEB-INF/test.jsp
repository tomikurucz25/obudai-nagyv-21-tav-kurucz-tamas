<link rel="stylesheet" type="text/css" href="css/form.css">

<div class="container">
  <form>
      <h2>Add recipe</h2>
      <div class="input-group input-group-icon">
        <input type="text" placeholder="Name"/>
        <div class="input-icon"><i class="fa fa-user"></i></div>
      </div>
      <div class="input-group input-group-icon">
        <input type="Number" placeholder="Servings"/>
        <div class="input-icon"><i class="fa fa-user"></i></div>
      </div>
      <div class="input-group input-group-icon">
        <textarea placeholder="Ingredients" rows="4" cols="62"></textarea>
      </div>
      <div class="input-group input-group-icon">
        <textarea placeholder="Preparation" rows="4" cols="62"></textarea>
      </div>
      <label class="select" for="slct">
  <select id="slct" required="required">
    <option value="" disabled="disabled" selected="selected">Select option</option>
    <option value="#">One</option>
    <option value="#">Two</option>
    <option value="#">Three</option>
    <option value="#">Four</option>
    <option value="#">Five</option>
    <option value="#">Six</option>
    <option value="#">Seven</option>
  </select>
  <svg>
    <use xlink:href="#select-arrow-down"></use>
  </svg>
</label>
		<div >
        <input type="submit" value="Save recipe">
      </div>
  </form>
    </div>
</div>