package cookbook.persistence.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
public class Cook extends User {
	public Cook(Long id, String username, String password) {
		this.id = id;
		this.username = username;
		this.password = password;
		isLoggedIn=false;
		ownRecipes = new ArrayList<Recipe>();
		comments = new ArrayList<Comment>();
	}
	
	public Cook() {
		
	}
	
	@OneToMany(mappedBy = "uploader")
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Recipe> ownRecipes;
	
	@OneToMany(mappedBy = "owner")
	private List<Comment> comments;

	@Transient
	private boolean isLoggedIn;
	public boolean getIsLoggedIn() {
		return isLoggedIn;
	}
	public void setIsLoggedIn(boolean value) {
		isLoggedIn = value;
	}
	
	public String getUsername() {
		return username;
	}
	
	public List<Recipe> getOwnRecipes() {
		return ownRecipes;
	}
	
	public Long getId() {
		return id;
	}
	
	public List<Comment> getComments() {
		return comments;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password=" + password + "]";
	}
}
