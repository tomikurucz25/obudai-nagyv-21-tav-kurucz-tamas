package cookbook.persistence.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;


@Entity
public class CookBook {

	public CookBook() {
		cooks = new ArrayList<Cook>();
		recipes = new ArrayList<Recipe>();
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@OneToMany
	private List<Cook> cooks;
	
	@OneToMany
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Recipe> recipes;

	public void setCooks(List<Cook> cooks) {
		this.cooks = cooks;
	}

	public List<Cook> getCooks() {
		return cooks;
	}

	public List<Recipe> getRecipes() {
		return recipes;
	}

	public void setRecipes(List<Recipe> recipes) {
		this.recipes = recipes;
	}
	
	
	
}
