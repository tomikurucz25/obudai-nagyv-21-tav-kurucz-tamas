package cookbook.persistence.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.FetchProfile.FetchOverride;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import cookbook.domain.Category;

@Entity
public class Recipe {

	public Recipe() {
		this.ingredients = new ArrayList<Ingredient>();
		this.categories = new ArrayList<Category>();
		this.comments = new ArrayList<Comment>();
	}
	
	public Recipe(String name, int servings, String preparation) {
		this.ingredients = new ArrayList<Ingredient>();
		this.categories = new ArrayList<Category>();
		this.comments = new ArrayList<Comment>();
	}
	
	public Recipe(Cook uploader, String name, String preparation,
			int servings, ArrayList<Ingredient> ingredients, ArrayList<Category> categories) {
		this.uploader = uploader;
		this.name = name;
		this.preparation = preparation;
		this.servings = servings;
		
		this.ingredients = ingredients;
		this.categories = categories;
		this.comments = new ArrayList<Comment>();
	}
    
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	private int servings;
	private String preparation;

    @ManyToOne
  	private Cook uploader;
  	
  	@OneToMany
  	@LazyCollection(LazyCollectionOption.FALSE)
  	private List<Ingredient> ingredients;
    
    @ElementCollection
    @Enumerated(EnumType.STRING)
    private List<Category> categories;
  	
  	@OneToMany
    private List<Comment> comments;
	
  	
	public int getServings() {
		return servings;
	}

	public Cook getUploader() {
		return uploader;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public String getPreparation() {
		return preparation;
	}

	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public List<Category> getCategories() {
		return categories;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public void setServings(int servings) {
		this.servings = servings;
	}

	public void setPreparation(String preparation) {
		this.preparation = preparation;
	}

	public void setUploader(Cook uploader) {
		this.uploader = uploader;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	@Override
	public String toString() {
		String ingredientsString = "";
		for (Ingredient ingredient : ingredients) {
			ingredientsString += "\t" + ingredient.getAmount() + " " + ingredient.getUnit() + " " + ingredient.getName() + "\n";
		}
		
		String categoriesString = "";
		for (Category category: categories) {
			categoriesString += "\t" + category.toString() + "\n";
		}
		
		return "-- Recipe: " + this.name + " --\nRecipe ID:\t" + this.id + "\nUploader:\t" + this.uploader.getUsername() + "\nServings:\t" + this.servings + "\nIngredients:\n" + ingredientsString  + "Preparation:\n" + this.preparation + "Categories:\n" + categoriesString;
	}
}
