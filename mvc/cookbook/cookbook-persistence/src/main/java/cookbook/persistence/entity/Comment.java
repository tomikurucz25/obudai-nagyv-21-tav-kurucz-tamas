package cookbook.persistence.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Comment {
	
	public Comment(Long id, Cook owner, String description, LocalDateTime timestamp) {
		this.id = id;
		this.owner = owner;
		this.description = description;
		this.timestamp = timestamp;
	}
	
	public Comment(Cook owner, String description) {
		this.owner = owner;
		this.description = description;
		this.timestamp = LocalDateTime.now();
	}

	public Comment() {
		
	}
    
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
    private String description;
    private LocalDateTime timestamp;

    @ManyToOne
    private Cook owner;
    

	public Long getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public Cook getOwner() {
		return owner;
	}
	
	

	public void setId(Long id) {
		this.id = id;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public void setOwner(Cook owner) {
		this.owner = owner;
	}

	@Override
	public String toString() {
		return timestamp + "\n" + description;
	}
}
