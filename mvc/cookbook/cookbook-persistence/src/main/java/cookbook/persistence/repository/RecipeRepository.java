package cookbook.persistence.repository;

import org.springframework.data.repository.CrudRepository;

import cookbook.persistence.entity.Recipe;

public interface RecipeRepository extends CrudRepository<Recipe, Long> {

}
