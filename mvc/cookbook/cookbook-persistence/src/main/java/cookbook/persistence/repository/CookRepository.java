package cookbook.persistence.repository;

import org.springframework.data.repository.CrudRepository;

import cookbook.persistence.entity.Cook;

public interface CookRepository extends CrudRepository<Cook, Long> {
	Cook findByUsername(String username);
}
