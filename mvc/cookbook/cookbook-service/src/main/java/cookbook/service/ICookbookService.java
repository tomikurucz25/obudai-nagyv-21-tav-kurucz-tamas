package cookbook.service;

import java.util.List;
import java.util.Set;
import cookbook.persistence.entity.*;
import cookbook.domain.*;

public interface ICookbookService {
    void addRecipe(Recipe recipe);
    void saveComment(Recipe recipe, String commentMessage);
    boolean isLoggedIn();
    List<Recipe> getRecipes();
    Cook getCurrentUser();
    void deleteRecipe(String asd);
}
