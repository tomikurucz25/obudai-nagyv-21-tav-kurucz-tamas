package cookbook.service.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import cookbook.persistence.entity.Cook;
import cookbook.persistence.repository.CookRepository;

@Service
public class CookbookUserDetailsService implements UserDetailsService {

	@Autowired
	private CookRepository cookRepository;

	@Override
	public UserDetails loadUserByUsername(String username) {
		Cook cook = cookRepository.findByUsername(username);
		if(cook == null)
		{
			throw new UsernameNotFoundException(username);
		}
		return new CookbookUserPrinciple(cook);
	}
}
