package cookbook.service.dto;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import cookbook.domain.Category;
import cookbook.persistence.entity.Comment;
import cookbook.persistence.entity.Cook;
import cookbook.service.validation.IngredientConstraint;

public class AddRecipeRequest {
	private Long id;
	@NotEmpty(message = "Name may not be empty ")
	private String name;
	@Positive
	@NotNull
	private int servings;
	@NotEmpty(message = "Preparation may not be empty ")
	private String preparation;
	private Cook uploader;
	@IngredientConstraint
	private String ingredients;
	@NotEmpty(message = "Categories may not be empty ")
	private List<Category> categories;
	private List<Comment> comments;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getServings() {
		return servings;
	}
	public void setServings(int servings) {
		this.servings = servings;
	}
	public String getPreparation() {
		return preparation;
	}
	public void setPreparation(String preparation) {
		this.preparation = preparation;
	}
	public Cook getUploader() {
		return uploader;
	}
	public void setUploader(Cook uploader) {
		this.uploader = uploader;
	}
	public String getIngredients() {
		return ingredients;
	}
	public void setIngredients(String ingredients) {
		this.ingredients = ingredients;
	}
	public List<Category> getCategories() {
		return categories;
	}
	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}
	public List<Comment> getComments() {
		return comments;
	}
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
}
