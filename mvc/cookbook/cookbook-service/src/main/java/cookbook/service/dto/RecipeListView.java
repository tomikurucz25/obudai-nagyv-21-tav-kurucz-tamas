package cookbook.service.dto;

import java.util.List;

import cookbook.domain.Category;
import cookbook.persistence.entity.Cook;

public class RecipeListView {
	private final Long id;
	private final String name;
	private final List<Category> categories;
	private final int servings;
	private final Cook uploader;
	
	public RecipeListView(Long id, String name, List<Category> categories, int servings, Cook uploader) {
		super();
		this.id = id;
		this.name = name;
		this.categories = categories;
		this.servings = servings;
		this.uploader = uploader;
	}

	public Long getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}

	public List<Category> getCategories() {
		return categories;
	}

	public int getServings() {
		return servings;
	}

	public Cook getUploader() {
		return uploader;
	}
}
