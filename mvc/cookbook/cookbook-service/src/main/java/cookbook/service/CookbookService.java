package cookbook.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import cookbook.persistence.entity.*;
import cookbook.persistence.repository.CommentRepository;
import cookbook.persistence.repository.CookBookRepository;
import cookbook.persistence.repository.CookRepository;
import cookbook.persistence.repository.IngredientRepository;
import cookbook.persistence.repository.RecipeRepository;
import cookbook.service.dto.RecipeFilters;
import cookbook.domain.*;

@Component
public class CookbookService implements ICookbookService {
	
	@Autowired
	RecipeRepository recipeRepository;
	
	@Autowired
	CookRepository cookRepository;
	
	@Autowired
	CommentRepository commentRepository;
	
	@Autowired
	IngredientRepository ingredientRepository;
	
	@Autowired
	CookBookRepository cookBookRepository;

	@Override
	public void addRecipe(Recipe recipe) {
		List<Ingredient> ingredients = recipe.getIngredients();
		recipe.getUploader().getOwnRecipes().add(recipe);
		CookBook cookBook = cookBookRepository.findById(Long.parseLong("1")).get();
		
		ingredientRepository.saveAll(ingredients);
		recipeRepository.save(recipe);
		cookBook.getRecipes().add(recipe);
	}

	@Override
	public void saveComment(Recipe recipe, String commentMessage) {
		Cook actualCook = getCurrentUser();
		Comment comment = new Comment(actualCook, commentMessage);
		actualCook.getComments().add(comment);
		recipe.getComments().add(comment);
		commentRepository.save(comment);
		cookRepository.save(actualCook);
		recipeRepository.save(recipe);
	}

	@Override
	public boolean isLoggedIn() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	    if (principal instanceof UserDetails) {
	    	String username = ((UserDetails)principal). getUsername();
	    	return true;
	    } else {
	    	return false;
	    }
	}

	@Override
	public List<Recipe> getRecipes() {
		var recipesIterable = recipeRepository.findAll();
		List<Recipe> recipes = new ArrayList<Recipe>();
		recipesIterable.forEach(r -> recipes.add(r));
		
		return recipes;
	}
	

	@Override
	public Cook getCurrentUser() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	    if (principal instanceof UserDetails) {
	    	String username = ((UserDetails)principal). getUsername();
	    	return cookRepository.findByUsername(username);
	    } else {
	    	return null;
	    }
	}


	@Override
	public void deleteRecipe(String recipeId) {
		Recipe recipe = recipeRepository.findById(Long.parseLong(recipeId)).get();
		CookBook cookBook = cookBookRepository.findById(Long.parseLong("1")).get();
		cookBook.getRecipes().size();
		cookBook.getRecipes().remove(recipe);
		recipe.getUploader().getOwnRecipes().remove(recipe);
		cookRepository.save(recipe.getUploader());
		cookBookRepository.save(cookBook);
		recipeRepository.delete(recipe);
		ingredientRepository.deleteAll(recipe.getIngredients());
		
	}

	public Recipe getRecipebyId(Long recipeId) {
		return recipeRepository.findById(recipeId).get();
	}

	public List<Ingredient> getIngredients(String ingredientTextArea) {
		List<Ingredient> ingredients = new ArrayList<Ingredient>();
		String[] lines = ingredientTextArea.split("\n");
		for (String ingredientString : lines) {
			String[] ingredientData = ingredientString.split(" ");
			double amount = Double.parseDouble(ingredientData[0]);
			Unit unit = Unit.valueOf(ingredientData[1]);
			String name = ingredientData[2];
			ingredients.add(new Ingredient(amount, unit, name));
		}
		return ingredients;
	}

	public List<Recipe> ActualUserRecipes() {
		var recipes = recipeRepository.findAll();
		List<Recipe> actualUserRecipes = new ArrayList<>();
		String actualUsername = getCurrentUser().getUsername();
		for (Recipe recipe : recipes) {
			if (recipe.getUploader().getUsername().equals(actualUsername)) {
				actualUserRecipes.add(recipe);
			}
		}
		return actualUserRecipes;
	}

	public List<Recipe> filteredRecipes(RecipeFilters recipeFilters) {
		List<Recipe> recipes = new ArrayList<Recipe>();
		var allRecipes = recipeRepository.findAll();
		boolean allFilterDisabled = !recipeFilters.isName()&&!recipeFilters.isCategory()&&!recipeFilters.isIngredient()&&!recipeFilters.isUploader();
		
		for (Recipe recipe : allRecipes) {
			if (allFilterDisabled||recipeFilters.isName()) {
				if (recipe.getName().contains(recipeFilters.getSearchInput())&&!recipes.contains(recipe)) {
					recipes.add(recipe);
				}	
			}
			if (allFilterDisabled||recipeFilters.isCategory()) {
				Category category;
				try {
					category = Category.valueOf(recipeFilters.getSearchInput());
				} catch (Exception e) {
					category = null;
				}
				if (recipe.getCategories().contains(category)&&!recipes.contains(recipe)) {
					recipes.add(recipe);
				}
			}
			if (allFilterDisabled||recipeFilters.isIngredient()) {
				List<String> ingredientNames = new ArrayList<String>();
				recipe.getIngredients().forEach(i -> ingredientNames.add(i.getName()));
				if (ingredientNames.contains(recipeFilters.getSearchInput())&&!recipes.contains(recipe)) {
					recipes.add(recipe);
				}
			}
			if (allFilterDisabled||recipeFilters.isUploader()) {
				if (recipe.getUploader().getUsername().equals(recipeFilters.getSearchInput())&&!recipes.contains(recipe)) {
					recipes.add(recipe);
				}
			}
		}
		return recipes;
	}
}
