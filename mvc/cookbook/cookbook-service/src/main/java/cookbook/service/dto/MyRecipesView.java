package cookbook.service.dto;

import cookbook.persistence.entity.Cook;

public class MyRecipesView {
	private final Long id;
	private final String name;
	private final int servings;
	private final Cook uploader;
	
	public MyRecipesView(Long id, String name, int servings, Cook uploader) {
		super();
		this.id = id;
		this.name = name;
		this.servings = servings;
		this.uploader = uploader;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getServings() {
		return servings;
	}

	public Cook getUploader() {
		return uploader;
	}
	
}
