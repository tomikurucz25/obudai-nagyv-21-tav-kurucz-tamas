package cookbook.service.transformer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cookbook.persistence.entity.Recipe;
import cookbook.service.CookbookService;
import cookbook.service.dto.AddRecipeRequest;
import cookbook.service.dto.MyRecipesView;
import cookbook.service.dto.RecipeDetailsView;
import cookbook.service.dto.RecipeListView;

@Component
public class RecipeTransformer {

	@Autowired
	CookbookService service;
	
	public void transform(Recipe recipe, RecipeDetailsView recipeDetailsView) {
		recipeDetailsView.setId(recipe.getId());
		recipeDetailsView.setName(recipe.getName());
		recipeDetailsView.setServings(recipe.getServings());
		recipeDetailsView.setPreparation(recipe.getPreparation());
		recipeDetailsView.setUploader(recipe.getUploader());
		recipeDetailsView.setCategories(recipe.getCategories());
		recipeDetailsView.setIngredients(recipe.getIngredients());
		recipeDetailsView.setComments(recipe.getComments());
	}
	
	public void transform(List<Recipe> recipes, List<RecipeListView> recipeListViews) {
		for (Recipe r : recipes) {
			RecipeListView recipeListView = new RecipeListView(r.getId(), r.getName(), r.getCategories(), r.getServings(), r.getUploader()); 
			recipeListViews.add(recipeListView);
		}
	}

	public void transform(AddRecipeRequest addRecipeRequest, Recipe recipe) {
		recipe.setId(addRecipeRequest.getId());
		recipe.setName(addRecipeRequest.getName());
		recipe.setServings(addRecipeRequest.getServings());
		recipe.setPreparation(addRecipeRequest.getPreparation());
		recipe.setUploader(service.getCurrentUser());
		recipe.setCategories(addRecipeRequest.getCategories());
		recipe.setIngredients(service.getIngredients(addRecipeRequest.getIngredients()));
		recipe.setComments(addRecipeRequest.getComments());
		
	}

}
