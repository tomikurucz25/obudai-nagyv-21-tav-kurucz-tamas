package cookbook.service.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = IngredientValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface IngredientConstraint {

	String message() default "Invalid ingredients";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
}
