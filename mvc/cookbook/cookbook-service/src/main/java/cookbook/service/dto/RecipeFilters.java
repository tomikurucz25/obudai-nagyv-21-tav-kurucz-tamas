package cookbook.service.dto;

public class RecipeFilters {
	private String searchInput;
	private boolean name;
	private boolean category;
	private boolean ingredient;
	private boolean uploader;
	
	public String getSearchInput() {
		return searchInput;
	}
	public void setSearchInput(String searchInput) {
		this.searchInput = searchInput;
	}
	public boolean isName() {
		return name;
	}
	public void setName(boolean name) {
		this.name = name;
	}
	public boolean isCategory() {
		return category;
	}
	public void setCategory(boolean category) {
		this.category = category;
	}
	public boolean isIngredient() {
		return ingredient;
	}
	public void setIngredient(boolean ingredient) {
		this.ingredient = ingredient;
	}
	public boolean isUploader() {
		return uploader;
	}
	public void setUploader(boolean uploader) {
		this.uploader = uploader;
	}
	
	
}
