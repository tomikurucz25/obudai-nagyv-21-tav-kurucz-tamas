package cookbook.service.validation;

import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import cookbook.domain.Unit;
import cookbook.persistence.entity.Ingredient;

public class IngredientValidator implements ConstraintValidator<IngredientConstraint, String> {
	
	@Override
	public boolean isValid(String ingredientsField, ConstraintValidatorContext ctx)
	{	
		String[] lines = ingredientsField.split("\n");
		if (lines.length==0) {
			return false;
		}
		for (String ingredientString : lines) {
			String[] ingredientData = ingredientString.split(" ");
			if (ingredientData.length!=3) {
				return false;
			}
			if(!ingredientData[0].matches("[0-9]+(\\.){0,1}[0-9]*"))
			{
				return false;
			}
			Unit unit;
			try {
				unit = Unit.valueOf(ingredientData[1]);
			} catch (Exception e) {
				unit=null;
			}
			if (unit==null) {
				return false;
			}
			String name = ingredientData[2];
			if (!name.matches("^[^0-9]*$")) {
				return false;
			}
		}
		
		return true;
	}

}
