package cookbook.persistence.repository;

import org.springframework.data.repository.CrudRepository;

import cookbook.persistence.entity.Comment;

public interface CommentRepository extends CrudRepository<Comment, Long> {	
	
}
