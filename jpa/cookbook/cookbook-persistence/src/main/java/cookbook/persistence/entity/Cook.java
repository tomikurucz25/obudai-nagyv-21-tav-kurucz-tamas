package cookbook.persistence.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

@Entity
public class Cook extends User {
	public Cook(Long id, String username, String password) {
		this.id = id;
		this.username = username;
		this.password = password;
		ownRecipes = new ArrayList<Recipe>();
		comments = new ArrayList<Comment>();
	}
	
	public Cook() {
		
	}
	
	@OneToMany(mappedBy = "uploader")
	private List<Recipe> ownRecipes;
	
	@OneToMany(mappedBy = "owner", fetch = FetchType.EAGER)
	private List<Comment> comments;
	
	
	public String getUsername() {
		return username;
	}
	
	public List<Recipe> getOwnRecipes() {
		return ownRecipes;
	}
	
	public Long getId() {
		return id;
	}
	
	public List<Comment> getComments() {
		return comments;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password=" + password + "]";
	}
}
