package cookbook.persistence.repository;

import org.springframework.data.repository.CrudRepository;

import cookbook.persistence.entity.CookBook;

public interface CookBookRepository extends CrudRepository<CookBook, Long> {

}
