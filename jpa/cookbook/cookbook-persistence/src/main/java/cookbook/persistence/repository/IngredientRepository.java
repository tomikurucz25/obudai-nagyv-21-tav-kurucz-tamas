package cookbook.persistence.repository;

import org.springframework.data.repository.CrudRepository;

import cookbook.persistence.entity.Ingredient;

public interface IngredientRepository extends CrudRepository<Ingredient, Long> {

}
