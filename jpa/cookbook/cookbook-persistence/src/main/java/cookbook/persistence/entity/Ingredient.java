package cookbook.persistence.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import cookbook.domain.Unit;

@Entity
public class Ingredient {
    
	public Ingredient(double amount, Unit unit, String name) {
		this.amount = amount;
		this.unit = unit;
		this.name = name;
	}
	
	public Ingredient() {
		
	}
	
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	double amount;
    String name;
    @Enumerated(EnumType.STRING)
    Unit unit;


	public double getAmount() {
		return amount;
	}

	public String getName() {
		return name;
	}

	public Unit getUnit() {
		return unit;
	}

	@Override
	public String toString() {
		return "Ingredient [amount=" + amount + ", name=" + name + ", unit=" + unit + "]";
	}
}
