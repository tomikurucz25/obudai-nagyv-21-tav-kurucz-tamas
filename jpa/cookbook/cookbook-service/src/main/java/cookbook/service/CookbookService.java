package cookbook.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cookbook.persistence.entity.*;
import cookbook.persistence.repository.CommentRepository;
import cookbook.persistence.repository.CookBookRepository;
import cookbook.persistence.repository.CookRepository;
import cookbook.persistence.repository.IngredientRepository;
import cookbook.persistence.repository.RecipeRepository;
import cookbook.service.*;
import cookbook.domain.*;

@Component
public class CookbookService implements ICookbookService {
	
	@Autowired
	RecipeRepository recipeRepository;
	
	@Autowired
	CookRepository cookRepository;
	
	@Autowired
	CommentRepository commentRepository;
	
	@Autowired
	IngredientRepository ingredientRepository;
	
	@Autowired
	CookBookRepository cookBookRepository;
	
	boolean isLoggedIn = false;
	Cook loggedInCook = null;

	@Override
	public void login(String usernamePassword) {
		String[] userData = usernamePassword.split(" ");
		User user = new Cook(null, userData[0], userData[1]);
		
		if (authenticate(user)) {
			isLoggedIn = true;
		}
	}

	@Override
	public void logout() {
		isLoggedIn = false;
		loggedInCook =  null;
	}

	@Override
	@Transactional
	public void addRecipe(Recipe recipe) {
		List<Ingredient> ingredients = recipe.getIngredients();
		ingredientRepository.saveAll(ingredients);
		CookBook cookBook = cookBookRepository.findById(Long.parseLong("1")).get();
		cookBook.getRecipes().size();
		cookBook.getRecipes().add(recipe);
		recipeRepository.save(recipe);
		cookBookRepository.save(cookBook);
	}

	@Override
	public void saveComment(Recipe recipe, String commentMessage) {
		Cook actualCook = recipe.getUploader();
		Comment comment = new Comment(actualCook, commentMessage);
		actualCook.getComments().add(comment);
		recipe.getComments().add(comment);
		commentRepository.save(comment);
		cookRepository.save(actualCook);
		recipeRepository.save(recipe);
	}

	@Override
	public boolean isLoggedIn() {
		return isLoggedIn;
	}

	@Override
	@Transactional
	public List<Recipe> getRecipes() {
		Iterable<Recipe> recipesIterable = recipeRepository.findAll();
		List<Recipe> recipes = new ArrayList<Recipe>();
		recipesIterable.forEach(recipes::add);
		return recipes;
	}
	
	@Override
	public Set<Category> getCategories() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Cook getCurrentUser() {
		return loggedInCook;
	}

	@Override
	public boolean authenticate(User user) {
		Iterable<Cook> cooksIterable = cookRepository.findAll();
		List<Cook> cooks = new ArrayList<Cook>();
		cooksIterable.forEach(cooks::add);
		for (Cook c : cooks) {
			if (c.getUsername().equals(user.getUsername())&&c.getPassword().equals(user.getPassword())) {
				loggedInCook = c;
				return true;
			}
		}
		return false;
	}

	@Override
	@Transactional
	public void deleteRecipe(String recipeId) {
		Recipe recipe = recipeRepository.findById(Long.parseLong(recipeId)).get();
		CookBook cookBook = cookBookRepository.findById(Long.parseLong("1")).get();
		cookBook.getRecipes().size();
		cookBook.getRecipes().remove(recipe);
		cookBookRepository.save(cookBook);
		
		recipe.getIngredients().size();
		ingredientRepository.deleteAll(recipe.getIngredients());
		
		recipeRepository.delete(recipe);
	}

	@Transactional
	public Recipe getRecipebyId(Long recipeId) {
		Recipe recipe = recipeRepository.findById(recipeId).get();
		recipe.getIngredients().size();
		recipe.getCategories().size();
		recipe.getComments().size();
		return recipe;
	}
}
